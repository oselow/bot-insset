# Bot insset

Pour utiliser ce bot merci de suivre les instructions suivantes :

-	Récupérer le repertoire
-	Créer un bot discord https://discord.com/developers/applications
-	Créer un ficher config.py à la racine du projet:
```py
token=<votreTokenDeBot>
log=<identifiant acces au site appel>
logPass=<mdp acces au site appel>
``` 
-	Installer les librairies 'discord', 'apscheduler', 'asyncio', 'bs4', 'requests'

Vous pouvez lancer votre bot dans un Docker ou un Virtualenv

Vous pouvez faire .initChannel <M1/M2> pour initialiser un appel
Vous pouvez faire .help pour avoir les autres commandes disponibles

A noter :
<br/>
Le bot ne peut avoir qu'un channel par classe.
<br/>
Le code est largement optimisable
<br/>
Les heures de rappel sont fixé à 8h30 et 14h30 donc les liens ne marcheront pas pour un appel à 10h30
<br/>
L'appel sera fait du jour où on initialise le channel à j+4 (du lundi au vendredi si on lance l'init le lundi)
<br/>
### Attention! Aucune vérification de droits est faite! Le bot se base sur la confiance des utilisateurs donc n'importe qui peut initialiser un channel