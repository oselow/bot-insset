import discord
from discord.ext import commands
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.schedulers.asyncio import AsyncIOScheduler
import datetime
import pytz
import json

from config import *
import asyncio

import os
import requests
from bs4 import BeautifulSoup


client = commands.Bot(command_prefix = ":")
client.remove_command('help')

scheduler = AsyncIOScheduler()
alertChannel = {}

@client.event
async def on_ready():
    print("Logged as : ", client.user.name)
    print("ID : ", client.user.id)    
    scheduler.start()
    # await rappel("M1", "202101251330", "04")
    # scheduler.resume()

@client.command()
async def help(ctx):
    await ctx.send(".notifyMe <M1/M2>       pour être notifié en message privé de l'appel des M1 ou M2")
    await ctx.send(".stopNotifyMe <M1/M2>   pour ne plus être notifié en message privé de l'appel des M1 ou M2")
    await ctx.send(".notifyList <M1/M2>     pour avoir la liste des personnes notifié en M1 ou M2")
    await ctx.send(".appels                 pour avoir l'historique des appels")
    await ctx.send(".status <M1/M2>         pour avoir les presents et non presents de l'appel en cours")
    await ctx.send(".iAm \"<nom prenom>\"   pour définir votre nom qui est sur le site de M. Drocourt")

@client.command()
async def notifyUser(ctx, arg1: discord.User, arg2):
    await notifyAdd(ctx, arg1.id, arg2)

@client.command()
async def notifyMe(ctx, arg):
    await notifyAdd(ctx, ctx.message.author.id, arg)

@client.command()
async def stopNotifyMe(ctx, arg):
    promo = arg
    user = ctx.message.author.id
    if (arg == "M1" or arg == "M2") :
        
        users = open("users.json", "r")
        json_object = json.load(users)
        users.close()

        if str(user) in json_object[promo].keys():
            del(json_object[promo][str(user)])

            users = open("users.json", "w")
            json.dump(json_object, users)
            users.close()
            await ctx.send('Vous ne serez plus notifié en message privé pour l\'appel ' + str(arg))
        else : 
            await ctx.send('Vous n\'êtiez déjà pas notifié en message privé pour l\'appel ' + str(arg))

        print(str(client.get_user(user).name) + " a fait la demande de ne plus être notifié pour l\'appel " + str(arg))
    else :
        print(str(client.get_user(user).name)+" a tenté de faire une demande de ne plus être notifié pour l\'appel " + str(arg))
        await ctx.send("Cette liste n'existe pas. Essayez avec M1 ou M2")

@client.command()
async def notifyList(ctx, arg):
    if (arg == "M1" or arg == "M2") :
        users = open("users.json", "r")
        json_object = json.load(users)
        users.close()

        message = "Liste : \n"
        for identifiant in json_object[arg].keys():
            if client.get_user(int(identifiant)) != None :
                message += str(identifiant) + " | " + client.get_user(int(identifiant)).name + " | " + str(json_object[arg][identifiant]) + "\n"
            else :
                message += "|" + str(identifiant) + "| utilisateur introuvable" + " | " + str(json_object[arg][identifiant]) + "\n"
        await ctx.send(message)
    else :
        await ctx.send("Cette liste n'existe pas. Essayez avec M1 ou M2")

@client.command()
async def appels(ctx):
    message = "Tâches : \n"
    for job in scheduler.get_jobs():
        message += "name: " + str(job.name) + " | trigger: " + str(job.trigger) + " | next run: " + str(job.next_run_time) + " | handler: " + str(job.func) + "\n"
    
    await ctx.send(message)

@client.command()
async def initChannel(ctx, arg):
    user = ctx.message.author.id
    if (arg == "M1" or arg == "M2") :
        global alertChannel
        alertChannel[str(arg)] = ctx

        if (scheduler.get_job('LOAD_LINKS_' + str(arg)) != None) :
            scheduler.remove_job('LOAD_LINKS_' + str(arg))
            
        local_time = pytz.timezone("Europe/Paris")

        today = datetime.datetime.now(local_time)
        monday = today - datetime.timedelta(days=today.weekday())
        naive_datetime = datetime.datetime.strptime(monday.strftime('%Y-%m-%d') + " 01:00:00", "%Y-%m-%d %H:%M:%S")
        local_datetime = local_time.localize(naive_datetime, is_dst=None)
        start = local_datetime.astimezone(pytz.utc)
        # print(start)
        
        scheduler.add_job(loadLinksofCurrentWeek,'interval', args=[ctx,arg], weeks=1, id=('LOAD_LINKS_' + str(arg)), start_date=start)
        
        await loadLinksofCurrentWeek(ctx,arg)
        
        print(client.get_user(user).name + " vient d'initialiser un channel pour l'appel " + str(arg) + " le " + today.strftime('%d-%m-%Y %H:%M:%S') )
    else :
        print(str(client.get_user(user).name)+" a tenté d'initialiser un channel pour l'appel " + str(arg))
        await ctx.send("Cette liste n'existe pas. Essayez avec M1 ou M2")
    
    
async def loadLinksofCurrentWeek(ctx, arg):
    week = str.zfill(str(datetime.datetime.today().isocalendar()[1]),2)
    
    requete = requests.get("https://ccm.insset.fr/emargements/" + arg + "/W" + week + "/", auth=(log, logPass))
    message = "Appels : \n"
    if requete.status_code != 404 :
        page = requete.content
        soup = BeautifulSoup(page,"html.parser")
        links = soup.find_all("a")

        for link in soup.find_all('a', href=True):
            dt = link['href'].replace('.','').replace('/','')
            if (scheduler.get_job('APPEL_' + str(arg) + "_" + dt) == None) :
                local_time = pytz.timezone("Europe/Paris")
                naive_datetime = datetime.datetime.strptime(dt, '%Y%m%d%H%M')
                local_datetime = local_time.localize(naive_datetime, is_dst=None)
                date = local_datetime.astimezone(pytz.utc)
                print(date.strftime('%Y-%m-%d %H:%M:%S') + "(UTC+1H)")
                scheduler.add_job(appel,'interval', args=[str(arg),dt,week], days=1, id=('APPEL_' + str(arg) + "_" + dt), start_date=date, end_date=date)
                message += "Appel le " + date.strftime('%Y-%m-%d %H:%M:%S') + " (UTC+1H) \n"
        await ctx.send(message)
    else :
        print("Pas d'appel pour la semaine " + week)


@client.command()
async def iAm(ctx, arg):
    user = ctx.message.author.id
    users = open("users.json", "r")
    json_object = json.load(users)
    users.close()
    # print(json_object)

    exist = False
    for liste in json_object.keys() :
        if str(user) in json_object[liste].keys():
            json_object[liste][str(user)] = arg
            exist = True

    if (exist) :
        users = open("users.json", "w")
        json.dump(json_object, users)
        users.close()
        await ctx.send('Ooooh, ravi de faire ta connaissance ' + arg)
    else :
        await ctx.send('Vous n\'êtes pas dans une liste de notifications')

@client.command()
async def status(ctx, arg, arg2):
    week = str.zfill(str(datetime.datetime.today().isocalendar()[1]),2)

    requete = requests.get("https://ccm.insset.fr/emargements/" + arg + "/W" + week + "/" + arg2 + "/check.php", auth=(log, logPass))
    message = "Status : \n"
    if requete.status_code != 404 :
        page = requete.content
        soup = BeautifulSoup(page,"html.parser")
        cases = soup.find_all("td")
        for i in range(0 , len(cases), 2):
            if (str(cases[i+1].text) != "") :
                message += (str(cases[i].text) + " => " + str(cases[i+1].text)) + "\n"

        await ctx.send(message)
    else :
        print("Pas d'appel pour la semaine " + week)

async def rappel(liste, dt, week):
    requete = requests.get("https://ccm.insset.fr/emargements/" + liste + "/W" + week + "/"+dt+"/check.php", auth=(log, logPass))
    if requete.status_code != 404 :
        users = open("users.json", "r")
        json_object = json.load(users)
        users.close()

        page = requete.content
        soup = BeautifulSoup(page,"html.parser")
        cases = soup.find_all("td")
        message = "Absents : \n"
        dm = "Tu as oublié l'appel " + ": http://ccm.insset.fr/emargements/" + str(liste) + "/W"+str(week)+"/"+str(dt)
        for i in range(0 , len(cases), 2):
            if (str(cases[i+1].text).lower() == "absent") :
                message += (str(cases[i].text) + " => " + str(cases[i+1].text)) + "\n"
                for identifiant in json_object[liste].keys():
                    if (str(cases[i].text)).lower() in json_object[liste][identifiant].lower() :
                        if client.get_user(int(identifiant)) != None :
                            try:
                                await client.get_user(int(identifiant)).send(dm)
                                print("DM envoyé à " + client.get_user(int(identifiant)).name)
                            except:
                                print("Impossible de DM " + client.get_user(int(identifiant)).name)

        # await alertChannel[liste].send(message)
        print(message)
    else :
        print("Pas d'appel pour la semaine " + week)

async def appel(liste, dt, week):
    await alertChannel[liste].send(liste + ": http://ccm.insset.fr/emargements/" + str(liste) + "/W"+str(week)+"/"+str(dt))
    await sendDmToList(liste, "Ne pas oublier l'appel " + ": http://ccm.insset.fr/emargements/" + str(liste) + "/W"+str(week)+"/"+str(dt))

    local_time = pytz.timezone("Europe/Paris")
    naive_datetime = datetime.datetime.strptime(dt, '%Y%m%d%H%M')
    local_datetime = local_time.localize(naive_datetime, is_dst=None)
    date = local_datetime.astimezone(pytz.utc)
    end = date + datetime.timedelta(hours=2)

    if (scheduler.get_job('RAPPEL_' + str(liste)) != None) :
        scheduler.remove_job('RAPPEL_' + str(liste))

    scheduler.add_job(rappel,'interval', args=[liste,dt,week], minutes=15, id=('RAPPEL_' + str(liste)), start_date=date, end_date=end)

async def sendDmToList(liste, message):
    users = open("users.json", "r")
    json_object = json.load(users)
    users.close()
    for identifiant in json_object[liste].keys():
        if client.get_user(int(identifiant)) != None :
            try:
                await client.get_user(int(identifiant)).send(message)
                print("DM envoyé à " + client.get_user(int(identifiant)).name)
            except:
                print("Impossible de DM " + client.get_user(int(identifiant)).name)
        else :
            print("|" + str(identifiant) + "| utilisateur introuvable, DM non envoyé")

async def notifyAdd(ctx, user, promo):
    if (promo == "M1" or promo == "M2") :

        users = open("users.json", "r")
        json_object = json.load(users)
        users.close()
        # print(json_object)

        if not str(user) in json_object[promo].keys():
            json_object[promo][str(user)] = ""

            users = open("users.json", "w")
            json.dump(json_object, users)
            users.close()
            await ctx.send('Vous serez notifié par message privé pour l\'appel ' + str(promo) )
        else : 
            await ctx.send('Vous êtes déjà sur la liste de notifications de l\'appel ' + str(promo))

        print(str(client.get_user(int(user)).name)+" a fait une demande de notification pour l\'appel " + str(promo))
    else :
        print(str(client.get_user(int(user)).name)+" a tenté une demande de notification pour l\'appel " + str(promo))
        await ctx.send("Cette liste n'existe pas. Essayez avec M1 ou M2")

# http://voss.u13.org/emargements/M1/W43/23PM

client.run(token)
