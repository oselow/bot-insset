FROM python:3

WORKDIR /data

RUN pip install install --upgrade pip \
    && pip install apscheduler \
    && pip install discord \
    && pip install asyncio \
    && pip install bs4 \
    && pip install requests

COPY . .

CMD [ "python","-u","./bot.py" ]
